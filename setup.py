from setuptools import setup
import pathlib

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / "README.md").read_text(encoding="utf-8")
setup(
    name="pykapsylen",
    version="1.0",
    description="PyKapsylen - create mosaic from beer bottle caps",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bohdan.bobrowski/pykapsylen",
    author="Bohdan Bobrowski",
    author_email="bohdan.bobrowski@codibly.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    keywords="",
    packages=["cli_parser", "kapsylen", "web_ui"],
    python_requires=">=3.6, <4",
    install_requires=["glob", "Pillow", "scipy", "numpy", "Flask", "Flask-Executor"],
    package_data={},
    data_files=[],
    entry_points={
        "console_scripts": [
            "kapsylen=kapsylen:main",
        ],
    },
)
