import os
import uuid
import time
import logging
import random
from flask import Flask, render_template, flash, request, redirect, send_from_directory
from flask_executor import Executor
from kapsylen import Kapsylen
from cli_parser import KapsylenArgumentParser

UPLOAD_FOLDER = "assets/uploads"
GENERATED_FOLDER = "assets/generated"
CAPS_FOLDER = "assets/caps"
ALLOWED_EXTENSIONS = set(["jpg", "jpeg"])

app = Flask(__name__)
executor = Executor(app)
app.secret_key = "kapsylen"
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["GENERATED_FOLDER"] = GENERATED_FOLDER
app.config["CAPS_FOLDER"] = CAPS_FOLDER
app.config["EXECUTOR_TYPE"] = "thread"
app.config["EXECUTOR_MAX_WORKERS"] = 5

KapsylenArgumentParser.logging_setup()


@app.route("/")
def kapsylen():
    caps_files = []
    for cap in os.listdir(os.path.abspath(app.config["CAPS_FOLDER"])):
        caps_files.append(cap)
    random_cap = random.choice(caps_files)
    return render_template("kapsylen.html", cap=random_cap)


@app.route("/generated/<path:filename>")
def generated_files(filename):
    return send_from_directory(
        os.path.join("..", app.config["GENERATED_FOLDER"]), filename
    )

@app.route("/caps/<path:filename>")
def caps_files(filename):
    return send_from_directory(
        os.path.join("..", app.config["CAPS_FOLDER"]), filename
    )


def allowed_file(filename: str) -> None:
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def delete_temporary_file(file: str, delay: int = 15) -> None:
    logging.info("In {} sec. temporary file {} will be deleted.".format(delay, file))
    time.sleep(delay)
    if os.path.exists(file):
        os.remove(file)
        logging.info("Temporary file {} was deleted.".format(file))


@app.route("/generate", methods=["POST"])
def generate():
    if "picture" not in request.files:
        flash("No file part")
    file = request.files["picture"]
    if file.filename == "":
        flash("No selected file")
        return redirect("/")
    if file and allowed_file(file.filename):
        filename = str(uuid.uuid4()) + ".jpg"
        image_file = os.path.abspath(
            os.path.join(app.config["UPLOAD_FOLDER"], filename)
        )
        file.save(image_file)
        output_file = os.path.abspath(
            os.path.join(app.config["GENERATED_FOLDER"], filename)
        )
        kapsylen_generator = Kapsylen(
            image_file=image_file,
            output_file=output_file,
            caps_folder=os.path.abspath(app.config["CAPS_FOLDER"]),
            caps_size=(30, 30),
        )
        kapsylen_generator.generate()
    executor.submit(delete_temporary_file, image_file)
    executor.submit(delete_temporary_file, output_file)
    return render_template(
        "generate.html", generated_file=os.path.join("generated", filename)
    )
