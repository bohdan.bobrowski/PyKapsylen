from cli_parser import KapsylenArgumentParser
from kapsylen import Kapsylen


def main():
    script_parser = KapsylenArgumentParser(description="", default_limit=7)
    args = script_parser.parse_args()
    kapsylen_generator = Kapsylen(
        args.image_file, args.output_file, args.caps_folder, (args.size, args.size)
    )
    kapsylen_generator.generate()


if __name__ == "__main__":
    main()
