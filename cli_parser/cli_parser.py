import argparse
import logging


class KapsylenArgumentParser(argparse.ArgumentParser):
    def __init__(self, description="", default_limit=None, **kwargs) -> None:
        super(KapsylenArgumentParser, self).__init__(description, **kwargs)
        self.add_argument(
            "image_file",
            type=str,
            help="",
        )
        self.add_argument(
            "output_file",
            type=str,
            help="",
        )
        self.add_argument(
            "caps_folder", nargs="?", type=str, help="caps folder path", default="./assets/caps/*.png"
        )
        self.add_argument(
            "-s",
            "--size",
            action="store",
            type=int,
            metavar="INT",
            help="tile size",
            default=100,
        )
        self.add_argument(
            "-d",
            "--debug",
            action="store_true",
            help="debug mode",
            default=False,
        )

    @staticmethod
    def logging_setup(debug=False):
        if debug:
            lvl = logging.DEBUG
        else:
            lvl = logging.INFO
        logging.basicConfig(
            level=lvl, format="%(asctime)s - %(levelname)s - %(message)s"
        )
        if debug:
            logging.debug("Logging level is DEBUG ({}).".format(lvl))

    def parse_args(self, args=None, namespace=None):
        args = super().parse_args(args, namespace)
        self.logging_setup(args.debug)
        return args
