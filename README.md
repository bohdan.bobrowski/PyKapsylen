# PyKapsylen

Small project which aims to create simple tool, which creates mosaic based on a given image from collection of beer caps.

<img src="https://gitlab.com/bohdan.bobrowski/PyKapsylen/-/raw/master/pykapsylen_logo.gif" />

## Web-UI

```shell
gunicorn web_ui.kapsylen:app --reload
```

Temporarily app is available on [pykapsylen.herokuapp.com](https://pykapsylen.herokuapp.com/).

## CLI

```shell
usage: [-h] [-s INT] [-d] image_file output_file [caps_folder]

positional arguments:
  image_file
  output_file
  caps_folder         caps folder path

optional arguments:
  -h, --help          show this help message and exit
  -s INT, --size INT  tile size
  -d, --debug         debug mode
```

## Examples

```shell
python kapsylen.py ./assets/img/Mona_Lisa.jpeg mona_lisa.jpg
python kapsylen.py ./assets/img/Lenna.jpeg lenna.jpg --size=10
```

## It is nothing more than military test site for:
- multithreading
- numpy, Pillow...
- Flask