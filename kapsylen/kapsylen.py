import logging
import os
import random

import PIL.Image
from PIL import Image
from scipy import spatial
import numpy as np
from concurrent import futures

MAX_WORKERS = 32


class Kapsylen:
    _image_file = None
    _image = None
    _image_width = None
    _image_height = None
    _output_file = None
    _caps_folder = None
    _caps_size = ()
    _caps = []
    _caps_original = []
    _colors = []
    _closest_caps = []

    def __init__(
        self, image_file: str, output_file: str, caps_folder: str, caps_size=(50, 50)
    ) -> None:
        self._image_file = image_file
        self._output_file = output_file
        self._caps_folder = caps_folder
        self._caps_size = caps_size
        self._prepare_data()

    def _prepare_data(self) -> None:
        self._load_caps()
        self._calculate_dominant_colors()
        self._load_image()
        self._pixelate_image()

    def _get_caps_paths(self) -> None:
        caps_paths = []
        for file in os.listdir(self._caps_folder):
            caps_paths.append(os.path.join(self._caps_folder, file))
        return caps_paths

    def _load_one_cap(self, path: str) -> None:
        logging.debug("Loading {}".format(path))
        cap = Image.open(path)
        self._caps.append(cap)

    def _load_caps(self) -> None:
        logging.info("Loading caps from {}".format(self._caps_folder))
        workers = min(MAX_WORKERS, len(self._get_caps_paths()))
        for path in self._get_caps_paths():
            with futures.ThreadPoolExecutor(workers) as executor:
                executor.submit(self._load_one_cap, path)
        logging.info("{} caps loaded".format(len(self._caps)))

    @staticmethod
    def _crop_cap(cap: PIL.Image.Image, margin: int = None) -> PIL.Image.Image:
        width, height = cap.size
        if margin is None:
            margin = round(width / 4)
        left = margin
        top = margin
        right = width - margin
        bottom = height - margin
        return cap.crop((left, top, right, bottom))

    def _calculate_color(self, cap: PIL.Image.Image) -> None:
        cropped = self._crop_cap(cap)
        cropped = cropped.convert(mode="P", colors=32, dither=1)
        cropped = cropped.convert("RGB")
        mean_color = np.array(cropped).mean(axis=0).mean(axis=0)
        if len(mean_color) == 4:
            mean_color = mean_color[:-1]
        self._colors.append(mean_color)
        hash_color = "#%02x%02x%02x" % tuple(mean_color.astype(np.int64))
        logging.debug("Dominant color calculated: {}".format(hash_color))

    def _calculate_dominant_colors(self) -> None:
        logging.info("Calculating caps dominant colors")
        self._colors = []
        workers = min(MAX_WORKERS, len(self._caps))
        for cap in self._caps:
            with futures.ThreadPoolExecutor(workers) as executor:
                executor.submit(self._calculate_color, cap)

    def _load_image(self) -> None:
        logging.info("Loading image from {}".format(self._image_file))
        self._image = Image.open(self._image_file)

    def _pixelate_image(self) -> None:
        logging.info("Pixelating source image.")
        self._image_width = int(np.round(self._image.size[0] / self._caps_size[0]))
        self._image_height = int(np.round(self._image.size[1] / self._caps_size[1]))
        resized_photo = self._image.resize((self._image_width, self._image_height))
        resized_photo = resized_photo.convert(colors=32, dither=1).convert("RGB")
        tree = spatial.KDTree(self._colors)
        self._closest_caps = np.zeros(
            (self._image_width, self._image_height), dtype=np.uint32
        )
        for i in range(self._image_width):
            for j in range(self._image_height):
                closest = tree.query(resized_photo.getpixel((i, j)))
                self._closest_caps[i, j] = closest[1]

    def _rotate_and_resize_cap(self, cap: PIL.Image.Image) -> PIL.Image.Image:
        rotated = cap.convert("RGBA").rotate(random.randrange(360))
        background = Image.new("RGBA", cap.size, (255,) * 4)
        rotated = Image.composite(rotated, background, rotated)
        rotated = rotated.convert(cap.mode)
        return rotated.resize(self._caps_size)

    def generate(self) -> None:
        logging.info("Creating output image.")
        output = Image.new(
            "RGB",
            (
                self._image_width * self._caps_size[0],
                self._image_height * self._caps_size[1],
            ),
        )
        for i in range(self._image_width):
            for j in range(self._image_height):
                x, y = i * self._caps_size[0], j * self._caps_size[1]
                index = self._closest_caps[i, j]
                output.paste(self._rotate_and_resize_cap(self._caps[index]), (x, y))
        output.save(self._output_file, quality=95)
        logging.info("Image {} saved.".format(self._output_file))
